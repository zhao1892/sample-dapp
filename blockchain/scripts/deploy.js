const { ethers } = require("hardhat")
require("dotenv").config({ path: ".env" });
const { WHITELIST_CONTRACT_ADDRESS, METADATA_URL, CRYPTO_DEVS_NFT_CONTRACT_ADDRESS, CRYPTO_DEV_TOKEN_CONTRACT_ADDRESS } = require("../constants");

async function main() {
    // const whitelistContract = await ethers.getContractFactory("Whitelist")
    // const deployedWhitelistContract = await whitelistContract.deploy(10)
    // await deployedWhitelistContract.deployed()

    // const whitelistContract = WHITELIST_CONTRACT_ADDRESS;
    // const metadataURL = METADATA_URL;
    // const cryptoDevsContract = await ethers.getContractFactory("CryptoDevs");
    // const deployedCryptoDevsContract = await cryptoDevsContract.deploy(metadataURL, whitelistContract);

    // const cryptoDevsNFTContract = CRYPTO_DEVS_NFT_CONTRACT_ADDRESS;
    //     // const cryptoDevsTokenContract = await ethers.getContractFactory("CryptoDevToken");
    //     // const deployedCryptoDevsTokenContract = await cryptoDevsTokenContract.deploy(cryptoDevsNFTContract)
    //     // console.log(
    //     //     "Contract Address:",
    //     //     deployedCryptoDevsTokenContract.address
    //     // );

    // const fakeNFTMarketplace = await ethers.getContractFactory("FakeNFTMarketplace");
    // const deployedFakeNftMarketplace = await fakeNFTMarketplace.deploy();
    // await deployedFakeNftMarketplace.deployed();
    // console.log("FakeNFTMarketplace deployed to: ", deployedFakeNftMarketplace.address);
    //
    // const cryptoDevsDAO = await ethers.getContractFactory("CryptoDevsDAO");
    // const deployedCryptoDevsDAO = await cryptoDevsDAO.deploy(
    //     deployedFakeNftMarketplace.address,
    //     cryptoDevsNFTContract,
    //     { value: ethers.utils.parseEther("0.01") }
    // );
    // await deployedCryptoDevsDAO.deployed();
    // console.log("CryptoDevsDAO deployed to: ", deployedCryptoDevsDAO.address);

    const cryptoDevTokenAddress = CRYPTO_DEV_TOKEN_CONTRACT_ADDRESS;
    const exchangeContract = await ethers.getContractFactory("Exchange");
    const deployedExchangeContract = await exchangeContract.deploy(cryptoDevTokenAddress);
    await deployedExchangeContract.deployed();
    console.log("Exchange Contract Address:", deployedExchangeContract.address);
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error)
        process.exit(1)
    })
